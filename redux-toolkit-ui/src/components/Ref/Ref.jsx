import React, {useRef} from 'react';
import Video from '../../../public/web_solution.mp4'

const Ref = () => {
    const vidRef = useRef(null);
    const playRef = useRef(false); // Додаємо стан для контролю відтворення

    const playVideo = () => {
        if (!playRef.current) {
            playRef.current = true;
            vidRef.current.play();
        }
    };

    const pauseVideo = () => {
        if (playRef.current) {
            playRef.current = false;
        }
    };

    const handleEnded = () => {
        playRef.current = false; // Скидаємо стан відтворення після завершення циклу
    };

    return (
        <div className="Ref" style={{width: '90px', height: '90px'}}>
            <video
                id="bannerVideo"
                onMouseOver={playVideo}
                onMouseOut={pauseVideo}
                onEnded={handleEnded}
                ref={vidRef}
                muted
                // loop
            >
                <source src={Video} type="video/mp4" />
            </video>
        </div>
    );
};

export default Ref;
