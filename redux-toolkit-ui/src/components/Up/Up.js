import React, {useContext} from 'react'
import useScroll from "../hooks/useScroll";
import ThemeContext from "../../Theme-context/ThemeContext";
import './Up.css'


const Up = () => {
    const{theme} = useContext(ThemeContext);
    const top = useScroll(500)
    if(top < 500) {
        return null
    }

    const handleUp = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        })
    }

    return (
        <div className={theme === "light" ? "up-light" : "up-dark"}>
            <button type="button" onClick={handleUp}>UP</button>
        </div>
    )
}

export default Up