import React, {useState, useEffect} from "react";

const useBreakpoint = () => {
    const [breakpoint, setBreakpoint] = useState('mobile');


    useEffect(() => {
        const handleResize = () => {
            if (window.innerWidth <= 480) {
                setBreakpoint('mobile');
            } else if (window.innerWidth <= 768) {
                setBreakpoint('tablet')
            } else {
                setBreakpoint('desktop')
            }
        }

        handleResize();

        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, []);

    return breakpoint
}

export default useBreakpoint


//Custom hook detect screen resize and return 'mobile', 'tablet' or 'desktop'