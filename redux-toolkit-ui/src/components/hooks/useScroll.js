import {useState, useEffect} from "react";
import debounce from "lodash.debounce";

const useScroll = (timeout = 200) => {
    const[top, setTop] = useState(0);

    const handleScroll = debounce(() => {
        setTop(window.scrollY);
    }, timeout)

    useEffect(() => {
        window.addEventListener("scroll", handleScroll);
        return () => {
            window.removeEventListener("scroll", handleScroll);
        }
    }, [])

    return top
}

export default useScroll

// кастомний хук для оптимизації скролла