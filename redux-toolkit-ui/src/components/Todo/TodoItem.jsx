import React, {memo, useState} from "react";
import PropTypes     from "prop-types";
import {deleteTodo, toggleTodo} from "../../redux/actionCreator/todoActionCreator";
import {useDispatch} from "react-redux";



const TodoItem = ({todo}) => {
const dispatch = useDispatch();

const [completed, setCompleted] = useState(todo.completed);

const handleComplete = () => {
    dispatch(toggleTodo(todo.id));
    setCompleted(todo.completed)
}

    return (
        <div className='flex justify-between items-center my-2'>
            <div className='text-sm px-4 py-2 cursor-pointer bg-lime-300 hover:bg-lime-400'
                 onClick={handleComplete }
            >
                {completed ? "Complete" : "Uncompleted" }
            </div>
            <div className={`text-sm ${todo.completed ? 'line-through font-medium text-lime-400' : ''}`}>
                {todo.title}
            </div>
            <div
                className='text-sm px-4 py-2 flex bg-red-400 hover:bg-red-500 transition-all text-white cursor-pointer'
                onClick={() => dispatch(deleteTodo(todo.id))}
            >
                Delete
            </div>
        </div>
    )
}

TodoItem.propTypes = {
    todo: PropTypes.object
}

TodoItem.defaultProps = {
    todo: {}
}

export default memo(TodoItem)