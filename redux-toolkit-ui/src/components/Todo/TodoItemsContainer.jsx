// import React from 'react';
import {shallowEqual, useSelector} from "react-redux";
import TodoItem from "./TodoItem";

const TodoItemsContainer = () => {
   const todos = useSelector((state) => state.todo.todos, shallowEqual);

    if(!todos || todos.length === 0)
       return  <div>No todos</div>

    return (
        <ul>
            {todos.map((todo, key) => (
                <li key={todo.id}>
                    <TodoItem todo={todo}/>
                </li>
            ))}
        </ul>
    )
}

export default TodoItemsContainer
