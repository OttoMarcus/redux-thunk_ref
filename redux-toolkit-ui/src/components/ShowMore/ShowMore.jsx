import React, {useRef, useEffect, useState} from 'react';

import './ShowMore.css';


const ShowMore = ({text}) => {
    const divRef = useRef(null);
    const [isShowMore, setIsShowMore] = useState(false);
    const [textHeight, setTextHeight] = useState(0);

    useEffect(() => {
        setTextHeight(divRef.current.getBoundingClientRect().height);
        // console.log(divRef.current.getBoundingClientRect().height)
    }, [divRef.current]);

    useEffect(() => {
        if (textHeight > 110) {
            setIsShowMore(true);
        }
    }, [textHeight]);

    return (
        <div className="showMore" ref={divRef}>
            <div className="textWrapper" style={{height: isShowMore ? 110 : 'auto'}}>
                <p className="textContainer">{text}</p>
            </div>
            {
                textHeight > 110 &&
                <button className="btn"
                        onClick={() => setIsShowMore(prev => !prev)}>{!isShowMore ? 'Show less' : 'Show more'}</button>
            }

        </div>

    )
}


export default ShowMore;
