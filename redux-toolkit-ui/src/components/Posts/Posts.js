import React, {useEffect} from 'react'
import PostItem from './PostItem'
import {useDispatch, useSelector} from "react-redux";
import {getPostsThunk} from "../../redux/actionCreator/postActionCreator";
import useBreakpoint from "../../components/hooks/useBreakpoint";


const  Posts = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.post.posts);
    const breakpoint = useBreakpoint();
    console.log(breakpoint);

    const breakPointMap = {
        mobile: 2,
        tablet: 4,
        desktop: 6
    }

    const cuttedPosts = posts.slice(0, breakPointMap[breakpoint]);

    useEffect(() => {
        dispatch(getPostsThunk());
    }, []);

    if (!posts && posts.length === 0 )
        return <p>There are not posts yet</p>


    return (
        <div>
            <button
                type='submit'
                className='bg-lime-300  hover:bg-lime-400 transition-all p-2 text-sm'
            >
                Get posts
            </button>
            {
                cuttedPosts.map((post, key) => (
                    <div key={post.id}>
                        <PostItem post={post}/>
                    </div>
                ))
            }
        </div>
    )
}

export default Posts
