import React, {useState, useRef, useContext} from 'react';
import Ref from "../Ref/Ref";
import './Header.css';
import ThemeContext from "../../Theme-context/ThemeContext";



const Header = () => {
    const [isSearch, setIsSearch] = useState(false);
    const searchRef = useRef(null);
    const timerRef = useRef(0);
    const {theme, toggleTheme} = useContext(ThemeContext);

    const handleSearch = () => {
    clearTimeout(timerRef.current);

    setIsSearch(prev => {
            if(!prev) {
                timerRef.current = setTimeout(() => searchRef.current.focus(), 400);
            }
            return !prev;
        });
    }


    const search = "search-field";
    const search_active = "search-field active";


    return (
        <div className={ theme==="light" ? "header-light" : "header"}>
            <Ref/>
            <button
                className={
                theme==='light' ?
                `bg-transparent
                hover:bg-lime-500 text-lime-500 font-semibold
                hover:text-white py-2 px-4 border border-lime-700
                hover:border-transparent rounded` :
                `bg-transparent
                hover:bg-blue-300 text-blue-200 font-semibold
                hover:text-white py-2 px-4 border border-blue-500
                hover:border-transparent rounded`
            }
                onClick={toggleTheme}
            >
                { theme ==="light" ? "Light mode" : "Dark mode" }
            </button>
            <button type="button" className="button-search" onClick={handleSearch}>Search</button>
            <input type="text" className={isSearch ? search_active : search} ref={searchRef}/>
        </div>
    );
};


export default Header;
