import React, { useState } from 'react'
import { useDispatch } from "react-redux";
import { addTodo } from "../../redux/actionCreator/todoActionCreator";

const Form = () => {
    const [inputValue, setInputValue] = useState("");
    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        if (inputValue.trim() !== "") {
            dispatch(addTodo(inputValue));
            setInputValue("");
        }
    }

    const handleChange = (e) => {
        setInputValue(e.target.value);
    }

    return (
        <form className='w-full flex' onSubmit={handleSubmit}>
            <input
                type='text'
                placeholder='Type something...'
                className='w-full p-1 focus:outline-none focus:border-lime-500 focus: border-2 placeholder:text-sm'
                value={inputValue}
                onChange={handleChange}
            />
            <button
                type='submit'
                className='shrink-0 bg-lime-300  hover:bg-lime-400 transition-all px-3 text-sm'
            >
                Submit
            </button>
        </form>
    )
}

export default Form
