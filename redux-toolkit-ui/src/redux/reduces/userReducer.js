import {SET_FIRSTNAME, SET_LASTNAME} from "../actions/userActions";


const initialValue = {
    firstName: "",
    lastName: ""
}

const userReducer = (state = initialValue, action) => {
    switch (action.type) {
        case SET_FIRSTNAME:
            {
                return {...state, firstName: action.payload}
            }
        case SET_LASTNAME:
        {
            return {...state, lastName: action.payload}
        }

        default:
            return state
    }
}

export default userReducer