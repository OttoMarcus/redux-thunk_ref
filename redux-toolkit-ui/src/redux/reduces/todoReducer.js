import { v4 as uuidv4 } from 'uuid';
import {ADD_TODO, DELETE_TODO, TOGGLE_TODO} from "../actions/todoActions";


const initialValue = {
    todos: [
        {
            id: 1,
            title: "Todo 1",
            completed: true
        },
        {
            id: 2,
            title: "Todo 2",
            completed: false
        },
        {
            id: 3,
            title: "Todo 3",
            completed: false
        },
        {
            id: 4,
            title: "Todo 4",
            completed: false
        }
    ]
}

const todoReducer = (state = initialValue, action) => {
    switch (action.type) {
       case ADD_TODO: {
           const newTodo = {
               id: uuidv4(),
               title: action.payload,
               completed: false
           }
           return {...state, todos: [...state.todos, newTodo]}
        }
        case DELETE_TODO: {
            const allTodos = state.todos;
            const newTodos = allTodos.filter(todo => todo.id !== action.payload);
            return {...state, todos: newTodos}
        }
        case TOGGLE_TODO: {
            const newTodos = state.todos;
            console.log(newTodos);
            const index = newTodos.findIndex(todo => todo.id === action.payload);
            newTodos[index].completed = !newTodos[index].completed;
            return {...state, todos: newTodos}
        }
        default:
            return state
    }
}

export default todoReducer