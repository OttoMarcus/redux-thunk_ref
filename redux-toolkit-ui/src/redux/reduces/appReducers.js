import {combineReducers} from "redux";
import userReducer from "./userReducer";
import todoReducer from "./todoReducer";
import postsReducer from "./postsReducer";

const appReducers = combineReducers({
    user: userReducer,
    todo: todoReducer,
    post: postsReducer
})

export default appReducers