import {SET_FIRSTNAME, SET_LASTNAME} from "../actions/userActions";

export const setFirstName = (firstName) => ({
    type: SET_FIRSTNAME,
    payload: firstName
})

export const setLastName = (lastName) => ({
    type: SET_LASTNAME,
    payload: lastName
})