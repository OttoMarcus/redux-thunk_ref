import {GET_POSTS} from "../actions/postActions";

export const getPosts = (posts) => ({ type: GET_POSTS, payload: posts})

export const getPostsThunk = () => async (dispatch) => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts')
    const data = await response.json();
    dispatch(getPosts(data));
}