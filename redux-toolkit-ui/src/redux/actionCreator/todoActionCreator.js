import {ADD_TODO, DELETE_TODO, TOGGLE_TODO} from "../actions/todoActions";

export const addTodo = (title) => ({
    type: ADD_TODO,
    payload: title
})

export const deleteTodo = (id) => ({
    type: DELETE_TODO,
    payload: id
})

export const toggleTodo = (id) => ({
    type: TOGGLE_TODO,
    payload: id
})