import React, {useState} from 'react'
import Form from './components/User/Form'
import User from './components/User/User'
import TodoItemsContainer from "./components/Todo/TodoItemsContainer";
import ShowMore from './components/ShowMore/ShowMore'
import Posts from './components/Posts/Posts'
import Header from "./components/Header/Header";
import ThemeContext from "./Theme-context/ThemeContext";
import './index.css'
import Up from "./components/Up/Up";



function App() {

    const text0 = "asfaeigeg;qeorjgoerghrgre jnsfh";

    const text = "By using a ref, you ensure that:\n" +
        "\n" +
        "You can store information between re-renders (unlike regular variables, which reset on every render).\n" +
        "Changing it does not trigger a re-render (unlike state variables, which trigger a re-render).\n" +
        "The information is local to each copy of your component (unlike the variables outside, which are shared).\n" +
        "Changing a ref does not trigger a re-render, so refs are not appropriate for storing information you want to display on the screen. Use state for that instead. Read more about choosing between useRef and useState.\n" +
        "\n" +
        "Examples of refer";

    const[theme, setTheme] = useState('light');

    const toggleTheme = () => {
        setTheme(prev => prev === 'light' ? 'dark' : 'light')
    };

    const value = {theme, toggleTheme};

    return (
        <ThemeContext.Provider value={value}>
            <div className={theme === 'light' ? "bg-lime-100" : "min-h-screen h-full w-screen bg-indigo-400"}>
                <Header/>
                <div className='container mx-auto px-4'>
                    <main className='flex gap-20 '>
                        <div className='w-1/3'>
                            <h1 className='font-bold my-5'>Redux Toolkit State Change</h1>
                            <User/>
                        </div>
                        <div className='w-1/3'>
                            <h1 className='font-bold my-5'>Redux Toolkit Todo App</h1>
                            <Form/>
                            <TodoItemsContainer/>
                        </div>
                        <div className='w-1/3'>
                            <h1 className='font-bold my-5'>Redux Toolkit Async Thunk</h1>
                            <Posts/>
                        </div>
                    </main>
                </div>
                <div className="ref-container">
                    <ShowMore text={text0}/>
                    <ShowMore text={text}/>
                </div>

            </div>
            <Up>Up</Up>
        </ThemeContext.Provider>
    )
}

export default App
